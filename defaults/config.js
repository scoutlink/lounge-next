"use strict";

module.exports = {
	// ## Server settings

	public: true,

	host: undefined,

	port: 9000,

	bind: undefined,

	reverseProxy: true,

	maxHistory: 10000,

	// ### `https`

	https: {
		enable: true,
		key: "ssl/cert.key",
		certificate: "ssl/cert.pem",
		ca: "ssl/ca.pem",
	},

	// ## Client settings

	theme: "main",

	prefetch: false,

	// ### `disableMediaPreview`
	//
	// When set to `true`, The Lounge will not preview media (images, video and
	// audio) hosted on third-party sites. This ensures the client does not
	// make any requests to external sites. If `prefetchStorage` is enabled,
	// images proxied via the The Lounge will be previewed.
	//
	// This has no effect if `prefetch` is set to `false`.
	//
	// This value is set to `false` by default.
	disableMediaPreview: false,

	// ### `prefetchStorage`

	prefetchStorage: false,

	prefetchMaxImageSize: 2048,

	// ### prefetchMaxSearchSize
	//
	// This value sets the maximum response size allowed when finding the Open
	// Graph tags for link previews. The entire response is temporarily stored
	// in memory and for some sites like YouTube this can easily exceed 300
	// kilobytes.
	//
	// This value is set to `50` kilobytes by default.
	prefetchMaxSearchSize: 50,

	// ### `fileUpload`
	//
	// Allow uploading files to the server hosting The Lounge.
	//
	// Files are stored in the `${THELOUNGE_HOME}/uploads` folder, do not expire,
	// and are not removed by The Lounge. This may cause issues depending on your
	// hardware, for example in terms of disk usage.
	//
	// The available keys for the `fileUpload` object are:
	//
	// - `enable`: When set to `true`, files can be uploaded on the client with a
	//   drag-and-drop or using the upload dialog.
	// - `maxFileSize`: When file upload is enabled, users sending files above
	//   this limit will be prompted with an error message in their browser. A value of
	//   `-1` disables the file size limit and allows files of any size. **Use at
	//   your own risk.** This value is set to `10240` kilobytes by default.
	// - `baseUrl`: If you want change the URL where uploaded files are accessed,
	//   you can set this option to `"https://example.com/folder/"` and the final URL
	//   would look like `"https://example.com/folder/aabbccddeeff1234/name.png"`.
	//   If you use this option, you must have a reverse proxy configured,
	//   to correctly proxy the uploads URLs back to The Lounge.
	//   This value is set to `null` by default.
	fileUpload: {
		enable: false,
		maxFileSize: 10240,
		baseUrl: null,
	},

	transports: ["polling", "websocket"],

	leaveMessage: "ScoutLink Webchat - https://webchat.scoutlink.net",

	defaults: {
		name: "ScoutLink",
		host: "",
		port: 6697,
		password: "",
		tls: true,
		rejectUnauthorized: true,
		nick: "Guest%%%%",
		username: "",
		realname: "ScoutLink Webchat",
		join: "",
	},

	displayNetwork: false,

	lockNetwork: true,

	messageStorage: ["sqlite", "text"],

	useHexIp: false,

	// ## WEBIRC support
	//
	webirc: null,

	identd: {
		enable: false,
		port: 113,
	},

	oidentd: null,

	ldap: {
		enable: false,

		url: "ldaps://example.com",

		tlsOptions: {},

		primaryKey: "uid",

		searchDN: {
			rootDN: "cn=thelounge,ou=system-users,dc=example,dc=com",
			rootPassword: "1234",

			filter: "(objectClass=person)(memberOf=ou=accounts,dc=example,dc=com)",

			base: "dc=example,dc=com",
			scope: "sub",
		},
	},

	// Don't enable any of these in prod
	debug: {
		ircFramework: false,

		raw: false,
	},
};
